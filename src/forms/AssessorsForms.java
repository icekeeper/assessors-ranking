package forms;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Степан on 08.03.2015.
 */
public class AssessorsForms {

    public static List<String> listFormFile(String fileName) {

        List<String> arrayList = new ArrayList<String>();

        File file = new File(fileName);

        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(file), "UTF-8"));

            String line = reader.readLine();
            while (line != null) {
                arrayList.add(line);
                line = reader.readLine();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        return arrayList;

    }


    public static void allInformation(List<String> list) {
        PrintWriter out = null;
        try {
            out = new PrintWriter(new FileWriter("D:\\Confidentially\\Assessors forms\\questionnaire1.txt"));

            for (int j = 0; j < list.size(); j++) {

                String[] p;
                p = list.get(j).split("\t");

                for (int i = 0; i < p.length; i++) {


                    out.println(" " + i + " --- " + p[i]);

                }
                out.println("-----------------------------");

            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            System.out.println("Record was successful");
            out.close();

        }


    }


    public static void someInformation(List<String> list, int number) {

        for (int j = 0; j < list.size(); j++) {

            String[] p;
            p = list.get(j).split("\t");

            System.out.println(" " + number + "---" + p[number]);


        }

    }


}
