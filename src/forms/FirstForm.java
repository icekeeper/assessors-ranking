package forms;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Степан on 23.03.2015.
 */
public class FirstForm {


    public static void getUniqueInformation() {

        PrintWriter out = null;
        try {
            out = new PrintWriter(new FileWriter("D:\\Confidentially\\Assessors forms\\unique1.txt"));
            List<String> allInfo;
            String fileName = "D:\\Confidentially\\Assessors forms\\taskset-export-122804.txt";
            allInfo = AssessorsForms.listFormFile(fileName);
            List<Set<String>> setList = new ArrayList<Set<String>>();
            Set<String> stringSet = new HashSet<String>();

            for (int i = 0; i < 29; i++) {


                for (String info : allInfo) {
                    String[] p;
                    p = info.split("\t");
                    if (p.length > i) {

                        stringSet.add(p[i]);
                    }

                }

                setList.add(stringSet);
                stringSet = new HashSet<String>();

            }

            for (Set<String> inList : setList) {
                for (String inSet : inList) {
                    out.println(inSet);
                }
                out.println("---------------------");
            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Record in unique1 was successful");
            out.close();
        }
    }

    public static void getImportantFeatures() {
        int number = 1;
        int globalNumber = 1;
        List<String> allData;
        PrintWriter out = null;
        List<Set<String>> setList = new ArrayList<Set<String>>();
        Set<String> stringSet = new HashSet<String>();
        try {
            out = new PrintWriter(new FileWriter("D:\\Confidentially\\Assessors forms\\unique1Finished.txt"));
            allData = AssessorsForms.listFormFile("D:\\Confidentially\\Assessors forms\\unique1.txt");
            Pattern pt = null;
            for (int i = 0; i < 17; i++) {

                if (i == 0) {
                    pt = Pattern.compile("(Место жительства)");
                }
                if (i == 1) {
                    pt = Pattern.compile("(Направление образования)");
                }
                if (i == 2) {
                    pt = Pattern.compile("(Я работаю асессором)");
                }
                if (i == 3) {
                    pt = Pattern.compile("(В школе я учился)");
                }
                if (i == 4) {
                    pt = Pattern.compile("(Основное место работы)");
                }
                if (i == 5) {
                    pt = Pattern.compile("(Возраст:)");
                }
                if (i == 6) {
                    pt = Pattern.compile("(Семейное положение:)");
                }
                if (i == 7) {
                    pt = Pattern.compile("(Дети:)");
                }
                if (i == 8) {
                    pt = Pattern.compile("(Образование:)");
                }
                if (i == 9) {
                    pt = Pattern.compile("(В вузе я учился)");
                }
                if (i == 10) {
                    pt = Pattern.compile("(Я читаю книги)");
                }
                if (i == 11) {
                    pt = Pattern.compile("(В основном я читаю)");
                }
                if (i == 12) {
                    pt = Pattern.compile("(Интернет-активность)");
                }
                if (i == 13) {
                    pt = Pattern.compile("(Покупки в интернете)");
                }
                if (i == 14) {
                    pt = Pattern.compile("(Онлайн игры)");
                }
                if (i == 15) {
                    pt = Pattern.compile("(Я смотрю онлайн или скачиваю фильмы)");
                }
                if (i == 16) {
                    pt = Pattern.compile("(Активность в социальных сетях)");
                }


                for (String line : allData) {
                    Matcher mt = pt.matcher(line);
                    if (mt.find()) {
                        stringSet.add(line);
                    }

                }
                setList.add(stringSet);
                stringSet = new HashSet<>();


            }
            //вывод в файл
            for (Set<String> inList : setList) {
                for (String inSet : inList) {
                    out.println(globalNumber + "." + number + "---" + inSet);
                    number++;
                }
                number = 1;
                globalNumber++;
            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Record in unique1Finished was successful");
            out.close();
        }
    }


}
