package forms;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Степан on 23.03.2015.
 */
public class SecondForm {
    public static void getUniqueInformation(){

        try (PrintWriter out = new PrintWriter(new FileWriter("D:\\Confidentially\\Assessors forms\\unique2Finished.txt"))) {
            List<String> allInfo;
            String fileName = "D:\\Confidentially\\Assessors forms\\taskset-export-122808.txt";
            allInfo = AssessorsForms.listFormFile(fileName);
            List<Set<String>> setList = new ArrayList<Set<String>>();
            Set<String> stringSet = new HashSet<String>();

            for (int i = 8; i < 29; i++) {


                for (String info : allInfo) {
                    String[] p;
                    p = info.split("\t");
                    if (p.length > i) {
                        stringSet.add(p[i]);
                    }

                }

                setList.add(stringSet);
                stringSet = new HashSet<String>();

            }
            //простой вывод множества
            //теперь надо пройись по списку и составить нормальное распределение по множеству важных факторов
            for (Set<String> inList : setList) {
                for (String inSet : inList) {
                    out.println(inSet);

                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
