package judge;

import java.io.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Степан on 16.03.2015.
 */
public class Judgement {

    public static List<String> listFormFile(long iter) {


        String fileName = "F:\\judgements.tsv";
        List<String> arrayList = new ArrayList<>();

        File file = new File(fileName);

        int countOfLine = 0;
        long count = iter * 100000;
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(file), "UTF-8"));

            String line = reader.readLine();
            line = reader.readLine();
            for (int i = 0; i < 120000000; i++) {
                line = reader.readLine();
            }
            while (countOfLine != count - 100000) {
                line = reader.readLine();
                countOfLine++;
            }

            while (countOfLine != count) {
                arrayList.add(line);
                line = reader.readLine();
                countOfLine++;
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        return arrayList;

    }

    public static void allInformationJudgements(List<String> list) {
        PrintWriter out = null;
        try {
            out = new PrintWriter(new FileWriter("D:\\Confidentially\\Judgements\\outputMax.txt"));

            for (int j = 0; j < list.size(); j++) {

                out.print(list.get(j));
                out.println();


            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            System.out.println("Record was successful");
            out.close();

        }


    }

}
