package regressionMethods.check;

import Jama.Matrix;
import regressionMethods.myVersionOfLearning.MultivariateLinear;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by Степан on 06.04.2015.
 */
public class Checking {
    public static void getCheck() {
        MultivariateLinear multivariateLinear = new MultivariateLinear();
        Map<String, double[]> construct = multivariateLinear.constructMatrixF("D:\\Confidentially\\Assessors forms\\SecondPartOftaskset-export-122804.txt", "D:\\Confidentially\\Marks\\secondPartOfAnswer.txt");
        double[][] constructF = new double[construct.size()][17];
        Queue<String> listOfLogin = new LinkedList<>();
        int k = 0;
        for (Map.Entry<String, double[]> entry : construct.entrySet()) {
            for (int j = 0; j < entry.getValue().length; j++) {
                constructF[k][j] = entry.getValue()[j];
            }
            listOfLogin.add(entry.getKey());//вытаскиваем логины в порядке заполнения матрицы F
            k++;
        }
        Matrix F = new Matrix(constructF);
        Matrix alpha = multivariateLinear.getRegression();
        Matrix answer = F.times(alpha);
        System.out.println("F");
        F.print(1, 1);
        System.out.println("answer");
        answer.print(1, 5);
        System.out.println("alpha");
        alpha.print(1,5);
        int i = 0;
        Map<String, Double> doubleMap = multivariateLinear.objectsAndAnswer("D:\\Confidentially\\Marks\\secondPartOfAnswer.txt");
        double[][] answerArray = answer.getArray();
        try (PrintWriter out = new PrintWriter(new FileWriter("D:\\Confidentially\\Assessors forms\\ResultOfTesting.txt"))) {
            System.out.println(" login    real    assumption");
            for (String tmp : listOfLogin) {
                out.println(tmp + " " + doubleMap.get(tmp) + " " + answerArray[i][0]);
                i++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static void getAccuracy() {
        List<String> listOfResult = MultivariateLinear.readMarksFromFile("D:\\Confidentially\\Assessors forms\\Features1_19_RelianceAndTime\\SMOreg.txt");
        int count = 0;
        double sum = 0;
        double sum1 = 0;
        for (String line : listOfResult) {
            String[] p;
            p = line.split(" ");
            double fact = Double.valueOf(p[1]);
            double methodResult = Double.valueOf(p[2]);
            //100 - round(avg(abs(fact – method_result) * 100/fact))
            if (fact == 0) {
                System.out.println(Math.pow(fact - methodResult, 2));
            } else {
                System.out.println(Math.pow(fact - methodResult, 2));
                sum = sum + Math.abs((fact - methodResult) * 100 / fact);
                sum1 = sum1 + Math.pow(fact - methodResult, 2);
                count++;
            }
        }

        System.out.println("accuracy = " + (100 - sum / count) + "%");
        System.out.println("deviation = " + Math.sqrt(sum1));


    }


}
