package regressionMethods.myVersionOfLearning;

import Jama.Matrix;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Created by Степан on 29.03.2015.
 */
public class MultivariateLinear {

    public static List<String> readMarksFromFile(String fileName) {
        List<String> arrayList = new ArrayList<String>();
        File file = new File(fileName);
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(file), "UTF-8"));
            String line = reader.readLine();
            while (line != null) {
                arrayList.add(line);
                line = reader.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public Map<String, Double> objectsAndAnswer(String nameOFPart) {

        List<String> listOfMarks = readMarksFromFile(nameOFPart);
        Map<String, Double> vector = new TreeMap<>();
        for (String line : listOfMarks) {
            String[] p;
            p = line.split(";");
            vector.put(p[0], Double.valueOf(p[1]));

        }

        return vector;
    }

    public Map<String, List<String>> createMatrixF(String nameOfPartForm) {

        List<String> allInfo = readMarksFromFile(nameOfPartForm);
        //тут надо считывать из файла, в котором только 80% логинов
        List<String> allUniqueInfo = readMarksFromFile("D:\\Confidentially\\Assessors forms\\unique1Finished.txt");
        Map<String, List<String>> matrixString = new TreeMap<>();
        List<String> matrixStringWithoutKey = new ArrayList<>();
        for (String lineInfo : allInfo) {
            String[] p;
            p = lineInfo.split("\t");
            for (int i = 0; i < p.length; i++) {

                if (p[i].isEmpty()) {
                    continue;
                }

                //отдельная обработка вариантов, в которых есть  ( ), например: В вузе я учился(лась)
                Pattern pt;
                Pattern pat2 = Pattern.compile("Я читаю книги");
                Matcher mat2 = pat2.matcher(p[i]);
                if (mat2.find()) {
                    String[] l;
                    l = p[i].split(":");
                    if (!(2 == l.length)) {
                        continue;
                    }
                    pt = Pattern.compile("Я читаю книги.*" + l[1]);
                } else {
                    Pattern pat1 = Pattern.compile("В вузе я учился");
                    Matcher mat1 = pat1.matcher(p[i]);
                    if (mat1.find()) {
                        String[] l;
                        l = p[i].split(":");
                        if (!(2 == l.length)) {
                            continue;
                        }
                        pt = Pattern.compile("В вузе я учился.*" + l[1]);
                    } else {
                        Pattern pat = Pattern.compile("Я смотрю онлайн или скачиваю фильмы");
                        Matcher mat = pat.matcher(p[i]);
                        if (mat.find()) {
                            String[] l;
                            l = p[i].split(":");
                            if (!(2 == l.length)) {
                                continue;
                            }
                            pt = Pattern.compile("Я смотрю онлайн или скачиваю фильмы.*" + l[1]);
                        } else {


                            Pattern pattern = Pattern.compile("В школе я учился");
                            Matcher matcher = pattern.matcher(p[i]);

                            if (matcher.find()) {
                                String[] l;
                                l = p[i].split(":");
                                if (!(2 == l.length)) {
                                    continue;
                                }
                                pt = Pattern.compile("В школе я учился.*" + l[1]);
                            } else {
                                try {
                                    pt = Pattern.compile(p[i]);
                                } catch (PatternSyntaxException e) {
                                    continue;
                                }
                            }
                        }
                    }
                }
                for (String line : allUniqueInfo) {
                    Matcher mt = pt.matcher(line);
                    if (mt.find()) {
                        String[] t;
                        t = line.split("---");
                        matrixStringWithoutKey.add(t[0]);
                    }

                }
            }
            matrixString.put(p[4], matrixStringWithoutKey);
            matrixStringWithoutKey = new ArrayList<>();
        }
        return matrixString;
        //верное постоение матрицы признаков
    }

    public void Read() {
        Map<String, List<String>> matrixString = createMatrixF("D:\\Confidentially\\Assessors forms\\FirstPartOftaskset-export-122804.txt");
        try (PrintWriter out = new PrintWriter(new FileWriter("D:\\Confidentially\\Assessors forms\\matrixF.txt"))) {
            for (Map.Entry<String, List<String>> entry : matrixString.entrySet()) {
                out.println(entry.getKey() + "/" + entry.getValue());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Map<String, double[]> constructMatrixF(String nameOfPartForm, String nameOfPartAnswer) {
        Map<String, double[]> resultMapWithLogin = new TreeMap<>();
        Map<String, List<String>> allMatrix = createMatrixF(nameOfPartForm);
        Map<String, Double> vectorOfAnswer = objectsAndAnswer(nameOfPartAnswer);
        List<Double> listOfDoubleMas = new ArrayList<>();
        //сначала надо пересеч эти списки
        vectorOfAnswer.keySet().retainAll(allMatrix.keySet());
        double[] constructMatrix = new double[17];
        System.out.println("newSize = " + vectorOfAnswer.size());
        //for (Map.Entry<String, List<String>> entry : allMatrix.entrySet()) {
        for (Map.Entry<String, Double> entry : vectorOfAnswer.entrySet()) {//достаем по ключу
            for (String elemOfmatrix : allMatrix.get(entry.getKey())) {
                listOfDoubleMas.add(Double.valueOf(elemOfmatrix));
            }
            Collections.sort(listOfDoubleMas);//сотировка строки матрицы
            System.out.println(" before: " + Arrays.toString(listOfDoubleMas.toArray()));
            //сначала нужно пройтись и избавиться от одиноковых записей, пример: 12.2, 12.4, 12.7
            List<Double> indexes = new ArrayList<>();
            for (double tmp : listOfDoubleMas) {
                int index = listOfDoubleMas.indexOf(tmp);
                if (index < listOfDoubleMas.size() - 1) {
                    double nextElem = listOfDoubleMas.get(index + 1);
                    if ((int) tmp == (int) nextElem) {
                        indexes.add((listOfDoubleMas.get(index + 1)));
                    }
                }
            }

            for (Double tmp : indexes) {
                listOfDoubleMas.remove(tmp);
            }
            //тут нужно добавить число в список, если его нет - тут нет 4-го признака [1.1, 3.1, 5.2, 6.6, 7.1, 8.3, 9.6, 12.2, 12.4, 12.7, 13.3, 14.3, 15.6, 17.6]
            List<Integer> numbers = new ArrayList<>();
            for (double tmp : listOfDoubleMas) {
                numbers.add((int) tmp);
            }
            for (int j = 1; j <= 17; j++) {
                if (!(numbers.contains(j))) {
                    listOfDoubleMas.add(j - 1, 0.0);
                }

            }

            System.out.println(" after: " + Arrays.toString(listOfDoubleMas.toArray()));
            for (int j = 0; j < 17; j++) {

                constructMatrix[j] = (listOfDoubleMas.get(j) * 10) % 10;

            }
            resultMapWithLogin.put(entry.getKey(), constructMatrix);
            constructMatrix = new double[17];
            listOfDoubleMas = new ArrayList<>();
        }
        /////////// вывод матрицы F - строки - объекты(асессоры), столбцы - признаки
        for (Map.Entry<String, double[]> entry : resultMapWithLogin.entrySet()) {
            System.out.println(entry.getKey() + "/" + Arrays.toString(entry.getValue()));
        }
        //////////
        return resultMapWithLogin;//правильно построенная матрица F
    }

    public Matrix getRegression() {

        Map<String, double[]> construct = constructMatrixF("D:\\Confidentially\\Assessors forms\\FirstPartOftaskset-export-122804.txt", "D:\\Confidentially\\Marks\\firstPartOfAnswer.txt");
        double[][] constructF = new double[construct.size()][17];
        //List<String> listOfLogin = new LinkedList<>();
        Queue<String> listOfLogin = new LinkedList<>();
        int k = 0;
        for (Map.Entry<String, double[]> entry : construct.entrySet()) {
            for (int j = 0; j < entry.getValue().length; j++) {
                constructF[k][j] = entry.getValue()[j];
            }
            listOfLogin.add(entry.getKey());//вытаскиваем логины в порядке заполнения матрицы F
            k++;
        }
        Matrix F = new Matrix(constructF);
        Matrix T = F.copy();
        System.out.println("F");
        F.print(1, 1);
        Matrix Ft = T.transpose();
        System.out.println("Ft");
        Ft.print(1, 1);
        Matrix FtF = Ft.times(F);
        System.out.println("FtF - это матрица ковариации");
        FtF.print(1, 1);


        System.out.println("матрица корреляции");
        double[][] corr = new double[17][17];
        double[][] ftf = FtF.getArray();
        for (int i = 0; i < 17; i++) {
            for (int j = 0; j < 17; j++) {
                corr[i][j] = ftf[i][j] / (Math.sqrt(ftf[i][i]) * Math.sqrt(ftf[j][j]));
            }
        }
        Matrix correl = new Matrix(corr);
        correl.print(1, 3);

        Matrix inverceFtF = FtF.inverse();
        //inverceFtF.print(1,1);
        Matrix inverseFtFFt = inverceFtF.times(Ft);
        Map<String, List<String>> allMatrix = createMatrixF("D:\\Confidentially\\Assessors forms\\FirstPartOftaskset-export-122804.txt");
        Map<String, Double> vectorOfAnswer = objectsAndAnswer("D:\\Confidentially\\Marks\\firstPartOfAnswer.txt");
        //сначала надо пересеч эти списки
        vectorOfAnswer.keySet().retainAll(allMatrix.keySet());
        //Double[] answer =(Double[])vectorOfAnswer.values().toArray(); такой cast не работает
        Double[] answer = new Double[vectorOfAnswer.size()];
        int index = 0;
        //достаем значение из вектора ответов для соответствующего объекта
        //тут из списка достаются логины в обратном порядке - надо так, но так ли это? хотя используется queue
        for (String tmp : listOfLogin) {
            answer[index] = vectorOfAnswer.get(tmp);
            index++;
        }
//        for (Map.Entry<String, Double> mapEntry : vectorOfAnswer.entrySet()) {
//            answer[index] = mapEntry.getValue();
//            index++;
//        }
        //ПРАВИЛЬНО ЛИ перемножаются ответы, в том ли порядке?
        double[][] toY = new double[answer.length][1];
        for (int i = 0; i < answer.length; i++) {
            toY[i][0] = answer[i];
        }
        //writelnMatrix(toY);
        Matrix y = new Matrix(toY);
        //y.print(1,1);
        Matrix alpha = inverseFtFFt.times(y);
        //alpha.print(1, 1);
        return alpha;
    }


}
