package regressionMethods.separate;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Степан on 05.04.2015.
 */
public class SeparationData {

    public static List<String> readFromFile(String fileName) {
        List<String> arrayList = new ArrayList<String>();
        File file = new File(fileName);
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(file), "UTF-8"));
            String line = reader.readLine();
            while (line != null) {
                arrayList.add(line);
                line = reader.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public static void separate() {
        List<String> listOfMarks = readFromFile("D:\\Confidentially\\Marks\\inputmarks.txt");
        List<String> listWithoutZero = new ArrayList<>();
        for (String line : listOfMarks) {
            String[] p;
            p = line.split(";");
            if (!(p[2].equals("0"))) {
                if (Double.valueOf(p[1]) == 0) {
                    String s = p[0] + ";" + "0.0";
                    listWithoutZero.add(s);
                } else {
                    String s = p[0] + ";" + String.valueOf(Double.valueOf(p[2]) / Double.valueOf(p[1]));
                    listWithoutZero.add(s);
                }
            }
        }

        double max = 0.0;
        double min = 0.0;

        for (String line : listWithoutZero) {
            String[] p;
            p = line.split(";");
            if (Double.valueOf(p[1]) > max) {
                max = Double.valueOf(p[1]);
            }
            if (Double.valueOf(p[1]) < min) {
                min = Double.valueOf(p[1]);
            }
        }

        List<String> maxListWithoutZero = new ArrayList<>();
        for (String line : listWithoutZero) {
            String[] p;
            p = line.split(";");
            String s = p[0] + ";" + String.valueOf(Double.valueOf(p[1]) + Math.abs(min));
            maxListWithoutZero.add(s);
        }

        max = max + Math.abs(min);
        min = 0.0;
        List<String> normListWithoutZero = new ArrayList<>();
        for (String line : maxListWithoutZero) {
            String[] p;
            p = line.split(";");
            String s = p[0] + ";" + String.valueOf(Double.valueOf(p[1]) / max);
            normListWithoutZero.add(s);
        }

        PrintWriter out1 = null;
        PrintWriter out2 = null;
        PrintWriter out3 = null;
        try {
            out1 = new PrintWriter(new FileWriter("D:\\Confidentially\\Marks\\firstPartOfAnswer.txt"));
            out2 = new PrintWriter(new FileWriter("D:\\Confidentially\\Marks\\secondPartOfAnswer.txt"));
            out3 = new PrintWriter(new FileWriter("D:\\Confidentially\\Marks\\thirdPartOfAnswer.txt"));
            double size = normListWithoutZero.size();
            double firstPart = size * 0.8;
            double secondPart = size * 0.1;
            for (int i = 0; i < (int) firstPart; i++) {
                out1.println(normListWithoutZero.get(i));
            }
            for (int i = (int) firstPart; i < (int) firstPart + secondPart; i++) {
                out2.println(normListWithoutZero.get(i));
            }
            for (int i = (int) (firstPart + secondPart); i < normListWithoutZero.size(); i++) {
                out3.println(normListWithoutZero.get(i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            out1.close();
            out2.close();
            out3.close();
        }
    }

    public static void separateProfile(String partOfAnswer, String nameFileOut) {
        List<String> FirstForm = readFromFile("D:\\Confidentially\\Assessors forms\\taskset-export-122804.txt");
        List<String> FirstAnswer = readFromFile(partOfAnswer);
        List<String> logins = new ArrayList<>();
        for (String line : FirstAnswer) {
            String[] p;
            p = line.split(";");
            logins.add(p[0]);
        }
        PrintWriter out = null;
        try {
            out = new PrintWriter(new FileWriter(nameFileOut));
            for (String line : FirstForm) {
                String[] p;
                p = line.split("\t");
                if (logins.contains(p[4])) {
                    out.println(line);
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            out.close();
        }

    }


}
