package regressionMethods.wekaProject;

import judge.Judgement;
import regressionMethods.myVersionOfLearning.MultivariateLinear;
import regressionMethods.separate.SeparationData;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Степан on 20.04.2015.
 */
public class AdditionFeatures {

    public static void createFileWithNewFeatures() {

        Map<String, Double> reliance = new HashMap<>();
        Map<String, List<Integer>> time = new HashMap<>();
        List<Integer> times = new ArrayList<>();

        String login = "first";
        boolean flag = false;
        int firstMonth = 0;
        int lastMinute = 0;
        int month = 0;
        int minute = 0;
        int count = 0;
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        Date date = null;

        PrintWriter out = null;


        try {
            out = new PrintWriter(new FileWriter("F:\\reliance4.txt"));


            List<Double> relOFAssessor = new ArrayList<>();

            for (int i = 1; i < 100; i++) {

                System.out.println(i);
                List<String> judgement = Judgement.listFormFile(i);

                for (String line : judgement) {
                    String[] p;
                    p = line.split("\t");


                    date = format.parse(p[4]);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    month = cal.get(Calendar.MONTH);
                    minute = cal.get(Calendar.MINUTE);

                    if (p[2].equals(login)) {

                        if (minute - lastMinute < 20) {
                            count++;
                        } else {
                            times.add(count);
                            count = 0;
                        }

                        if (!flag) {
                            firstMonth = month;
                            flag = true;
                        }
                        if ((month - firstMonth) == 1) {
                            relOFAssessor.add(Double.valueOf(p[5]));
                        }
                    } else {
                        double sum = 0;
                        for (double d : relOFAssessor) {
                            sum = sum + d;
                        }
                        reliance.put(login, sum / relOFAssessor.size());
                        time.put(login, times);
                        times = new ArrayList<>();
                        relOFAssessor = new ArrayList<>();
                        flag = false;
                    }
                    login = p[2];
                    lastMinute = minute;
                }
            }

            int i = 0;
            for (Map.Entry<String, Double> entry : reliance.entrySet()) {
                out.println(entry.getKey() + "/" + entry.getValue() + "/" + Arrays.toString(time.get(entry.getKey()).toArray()));
                i++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }

    }


    private static double[] concatArray(double[] a, double[] b) {
        if (a == null)
            return b;
        if (b == null)
            return a;
        double[] r = new double[a.length + b.length];
        System.arraycopy(a, 0, r, 0, a.length);
        System.arraycopy(b, 0, r, a.length, b.length);
        return r;
    }


    public static Map<String, double[]> addFeaturesFromSecondQuestionnaire(String PartOftask, String PartOfAnswer) {
        Map<String, double[]> matrixF = PrepareData.insertIntoFAverage(PartOftask, PartOfAnswer);
        Map<String, double[]> matrixFNew = new TreeMap<>();

        for (Map.Entry<String, double[]> entry : matrixF.entrySet()) {
            System.out.println(entry.getKey() + "/" + Arrays.toString(entry.getValue()));
        }
        List<String> secondQuestionnaire = SeparationData.readFromFile("D:\\Confidentially\\Assessors forms\\taskset-export-122808.txt");

        double[] newFeatures = new double[21];
        double[] relianceAndTime = new double[2];

        for (Map.Entry<String, double[]> entry : matrixF.entrySet()) {

            for (String line : secondQuestionnaire) {
                String[] p;
                p = line.split("\t");
                //p[4] - логин
                if (p[4].equals(entry.getKey())) {
                    for (int i = 8; i < 29; i++) {
                        String[] m;
                        m = p[i].split(":");
                        //m[1] нужно добавить в матрицу F
                        newFeatures[i - 8] = Double.valueOf(m[1]);
                    }
                }
            }

            double[] getFeatures18and19= new double[2];
            getFeatures18and19[0] = newFeatures[0];
            getFeatures18and19[1] = newFeatures[1];
            List<String> judgement = SeparationData.readFromFile("F:\\allReliance.txt");
            double sum = 0;
            double count = 0;

            for (String line : judgement) {
                String[] p;
                p = line.split("/");
                if (p[0].equals(entry.getKey())) {
                    if ((p[1].equals("Nan")) || (Double.valueOf(p[1]) > 1)) {
                        relianceAndTime[0] = 0.5;
                    } else {
                        relianceAndTime[0] = Double.valueOf(p[1]);
                    }
                    String[] m;
                    String t;
                    t = p[2].replaceAll("\\[", "");
                    t = t.replaceAll("\\]", "");
                    t = t.replaceAll(" ", "");
                    m = t.split(",");
                    //сделаем признак - средняя частота выставления оценок
                    for (int i = 0; i < m.length - 1; i++) {
                        if ((Double.valueOf(m[i + 1]) - Double.valueOf(m[i]) > 3 * Double.valueOf(m[i]))) {
                            count++;
                        }

                    }

                    sum = count / 5000.0;
                    relianceAndTime[1] = sum;
                    sum = 0;
                    count = 0;
                }
            }

            double[] arr = concatArray(getFeatures18and19, relianceAndTime);

            matrixF.put(entry.getKey(), concatArray(entry.getValue(), arr));
//            matrixFNew.put(entry.getKey(), newFeatures);
            newFeatures = new double[21];

        }

        for (Map.Entry<String, double[]> entry : matrixF.entrySet()) {
            System.out.println(entry.getKey() + "/" + Arrays.toString(entry.getValue()));
        }

        return matrixF;
//        return matrixFNew;
    }


}
