package regressionMethods.wekaProject;

/**
 * Created by Степан on 13.04.2015.
 */

import regressionMethods.myVersionOfLearning.MultivariateLinear;
import weka.classifiers.functions.*;
import weka.core.Instance;
import weka.core.Instances;

import javax.sound.sampled.Line;
import java.io.*;
import java.util.Map;

public class MakeRegression {
    public static void main(String args[]) throws Exception {
        //load data
        Instances data = new Instances(new BufferedReader(new
                FileReader("D:\\Confidentially\\Assessors forms\\FileToWeka1_19_reliance_time.txt")));
        data.setClassIndex(data.numAttributes() - 1);

        String[] logins = new String[48];
        double[] value = new double[48];
        int j = 0;
        MultivariateLinear multivariateLinear = new MultivariateLinear();
        Map<String, Double> vectorOfAnswer = multivariateLinear.objectsAndAnswer("D:\\Confidentially\\Marks\\secondPartOfAnswer.txt");
        for (Map.Entry<String, Double> entry : vectorOfAnswer.entrySet()) {
            logins[j] = entry.getKey();
            value[j] = entry.getValue();
            if (j != 47) {
                j++;
            }
        }
        //build model
       GaussianProcesses model = new GaussianProcesses();
        for (String option : model.getOptions()){
            System.out.println(option);
        }
        model.buildClassifier(data); //the last instance with missing
        //class is not used
        System.out.println(model);

        //48 тестовых асессоров от 345 до 392 включая оба
        int countOfInstance = data.numInstances();
        System.out.println(countOfInstance);
        //classify the last instance

        try (PrintWriter out = new PrintWriter(new FileWriter("D:\\Confidentially\\Assessors forms\\Features1_19_RelianceAndTime\\GaussianProcesses.txt"))) {
            int i = 344;
            j = 0;
            while (i != 392) {
                Instance assessor = data.instance(i);
                double result = model.classifyInstance(assessor);
                System.out.println("Assessor " + logins[j] + "(" + assessor + "): " + result);
                out.println(logins[j] + " " + value[j] + " " + result);
                i++;
                j++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
