package regressionMethods.wekaProject;

import Jama.Matrix;
import regressionMethods.myVersionOfLearning.MultivariateLinear;
import regressionMethods.separate.SeparationData;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * Created by Степан on 19.04.2015.
 */
public class PrepareData {

    public static Map<String, double[]> insertIntoFAverage(String PartOftask, String PartOfAnswer) {
        double[] average = {6.0, 3.5, 3.5, 3.5, 2.5, 4.5, 1.5, 2.0, 3.0, 2.0, 3.0, 9.5, 2.5, 3.5, 3.5, 2.5, 4.0};
        MultivariateLinear multivariateLinear = new MultivariateLinear();
        Map<String, double[]> resultMapWithLogin = multivariateLinear.constructMatrixF(PartOftask, PartOfAnswer);
        for (Map.Entry<String, double[]> entry : resultMapWithLogin.entrySet()) {
            for (int i = 0; i < entry.getValue().length; i++) {
                if (entry.getValue()[i] == 0.0) {
                    entry.getValue()[i] = average[i];
                }
            }
        }

        return resultMapWithLogin;
    }

    public static void showMatrixCorr() {
        Map<String, double[]> matrixF = AdditionFeatures.addFeaturesFromSecondQuestionnaire("D:\\Confidentially\\Assessors forms\\FirstPartOftaskset-export-122804.txt", "D:\\Confidentially\\Marks\\firstPartOfAnswer.txt");
        double[][] f = new double[matrixF.size()][matrixF.values().iterator().next().length];
        System.out.println("" + matrixF.size());
        System.out.println("" + matrixF.values().iterator().next().length);
        int k = 0;
        for (Map.Entry<String, double[]> entry : matrixF.entrySet()) {
            for (int i = 0; i < entry.getValue().length; i++) {
                f[k][i] = entry.getValue()[i];
            }
            k++;
        }
        Matrix F = new Matrix(f);
        Matrix T = F.copy();
        System.out.println("F");
        F.print(1, 1);
        Matrix Ft = T.transpose();
        System.out.println("Ft");
        Ft.print(1, 1);
        Matrix FtF = Ft.times(F);
        System.out.println("FtF - это матрица ковариации");
        FtF.print(1, 1);
        int size = matrixF.values().iterator().next().length;
        System.out.println("матрица корреляции");
        double[][] corr = new double[size][size];
        double[][] ftf = FtF.getArray();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                corr[i][j] = ftf[i][j] / (Math.sqrt(ftf[i][i]) * Math.sqrt(ftf[j][j]));
            }
        }
        Matrix correl = new Matrix(corr);
        correl.print(1, 3);


        System.out.println("ошибка коэффициента корреляции сигмаR");
        double[][] dover = new double[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < i; j++) {
                dover[i][j] = Math.sqrt(1 - corr[i][j] * corr[i][j]) / (Math.sqrt(size - 2));
            }
        }

        Matrix dov = new Matrix(dover);
        dov.print(1, 3);


        double[][] zero = new double[size][size];
        double[] countsI = new double[size];
        double[] countsJ = new double[size];
        System.out.println("попадает ли 0 в доверительный интервал");
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < i; j++) {
                if (((corr[i][j] - 2 * dover[i][j]) < 0) && ((corr[i][j] + 2 * dover[i][j]) > 0)) {
                    System.out.println("при i=" + i + " ,j=" + j + "  коэффициент корреляции является статистически значимым");
                    zero[i][j] = 1;
                    countsI[i]++;
                    countsJ[j]++;
                }
            }
        }

        Matrix zeros = new Matrix(zero);
        zeros.print(1, 1);

        for (int i = 0; i < size; i++) {
            System.out.print("  " + i);
        }
        System.out.println();
        for (int i = 0; i < size; i++) {
            System.out.print(" " + countsI[i]);
        }
        System.out.println();
        for (int j = 0; j < size; j++) {
            System.out.print(" " + countsJ[j]);
        }


    }

    public static void createTrainFileToWeka() {
        MultivariateLinear multivariateLinear = new MultivariateLinear();
        Map<String, Double> vectorOfAnswer = multivariateLinear.objectsAndAnswer("D:\\Confidentially\\Marks\\firstPartOfAnswer.txt");
//        Map<String, double[]> matrixF = insertIntoFAverage("D:\\Confidentially\\Assessors forms\\FirstPartOftaskset-export-122804.txt", "D:\\Confidentially\\Marks\\firstPartOfAnswer.txt");
        Map<String, double[]> matrixF = AdditionFeatures.addFeaturesFromSecondQuestionnaire("D:\\Confidentially\\Assessors forms\\FirstPartOftaskset-export-122804.txt", "D:\\Confidentially\\Marks\\firstPartOfAnswer.txt");
        try (PrintWriter out = new PrintWriter(new FileWriter("D:\\Confidentially\\Assessors forms\\FileToWeka1_19_reliance_time.txt"))) {
            out.println("@RELATION asessors");
            //(k == 20 || k == 21 || k == 23 || k == 25 || k == 26 || k == 27 || k == 28 || k == 31 || k == 34 || k == 36)
            out.println("@ATTRIBUTE att1 NUMERIC");
            out.println("@ATTRIBUTE att2 NUMERIC");
            out.println("@ATTRIBUTE att3 NUMERIC");
            out.println("@ATTRIBUTE att4 NUMERIC");
            out.println("@ATTRIBUTE att5 NUMERIC");
            out.println("@ATTRIBUTE att6 NUMERIC");
            out.println("@ATTRIBUTE att7 NUMERIC");
            out.println("@ATTRIBUTE att8 NUMERIC");
            out.println("@ATTRIBUTE att9 NUMERIC");
            out.println("@ATTRIBUTE att10 NUMERIC");
            out.println("@ATTRIBUTE att11 NUMERIC");
            out.println("@ATTRIBUTE att12 NUMERIC");
            out.println("@ATTRIBUTE att13 NUMERIC");
            out.println("@ATTRIBUTE att14 NUMERIC");
            out.println("@ATTRIBUTE att15 NUMERIC");
            out.println("@ATTRIBUTE att16 NUMERIC");
            out.println("@ATTRIBUTE att17 NUMERIC");
            ////////////17/////////////////////////
            out.println("@ATTRIBUTE att18 NUMERIC");
            out.println("@ATTRIBUTE att19 NUMERIC");
//            out.println("@ATTRIBUTE att20 NUMERIC");
//            out.println("@ATTRIBUTE att21 NUMERIC");
//            out.println("@ATTRIBUTE att22 NUMERIC");
//            out.println("@ATTRIBUTE att23 NUMERIC");
//            out.println("@ATTRIBUTE att24 NUMERIC");
//            out.println("@ATTRIBUTE att25 NUMERIC");
//            out.println("@ATTRIBUTE att26 NUMERIC");
//            out.println("@ATTRIBUTE att27 NUMERIC");
//            out.println("@ATTRIBUTE att28 NUMERIC");
//            out.println("@ATTRIBUTE att29 NUMERIC");
//            out.println("@ATTRIBUTE att30 NUMERIC");
//            out.println("@ATTRIBUTE att31 NUMERIC");
//            out.println("@ATTRIBUTE att32 NUMERIC");
//            out.println("@ATTRIBUTE att33 NUMERIC");
//            out.println("@ATTRIBUTE att34 NUMERIC");
//            out.println("@ATTRIBUTE att35 NUMERIC");
//            out.println("@ATTRIBUTE att36 NUMERIC");
//            out.println("@ATTRIBUTE att37 NUMERIC");
//            out.println("@ATTRIBUTE att38 NUMERIC");
//            out.println("@ATTRIBUTE att39 NUMERIC");
            out.println("@ATTRIBUTE reliance NUMERIC");
            out.println("@ATTRIBUTE time NUMERIC");
            out.println("@ATTRIBUTE result NUMERIC");
            out.println("@DATA");

            for (Map.Entry<String, double[]> entry : matrixF.entrySet()) {
                out.println();
                for (double elem : entry.getValue()) {
                    out.print(elem + ",");
                }
                out.print(vectorOfAnswer.get(entry.getKey()));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void createTestFileToWeka() {
        MultivariateLinear multivariateLinear = new MultivariateLinear();
        Map<String, Double> vectorOfAnswer = multivariateLinear.objectsAndAnswer("D:\\Confidentially\\Marks\\secondPartOfAnswer.txt");
//        Map<String, double[]> matrixF = insertIntoFAverage("D:\\Confidentially\\Assessors forms\\SecondPartOftaskset-export-122804.txt", "D:\\Confidentially\\Marks\\secondPartOfAnswer.txt");
        Map<String, double[]> matrixF = AdditionFeatures.addFeaturesFromSecondQuestionnaire("D:\\Confidentially\\Assessors forms\\SecondPartOftaskset-export-122804.txt", "D:\\Confidentially\\Marks\\secondPartOfAnswer.txt");
        try (PrintWriter out = new PrintWriter(new FileWriter("D:\\Confidentially\\Assessors forms\\FileToWeka1_19_reliance_time.txt", true))) {
            for (Map.Entry<String, double[]> entry : matrixF.entrySet()) {
                out.println();
                for (double elem : entry.getValue()) {
                    out.print(elem + ",");
                }
                System.out.println(entry.getKey() + " " + vectorOfAnswer.get(entry.getKey()));
                out.print("?");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
