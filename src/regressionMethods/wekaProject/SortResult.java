package regressionMethods.wekaProject;

import regressionMethods.separate.SeparationData;

import java.util.*;

/**
 * Created by Степан on 19.04.2015.
 */
public class SortResult {

    public static void main(String[] args) {
        getSorting();
    }

    public static void getSorting() {
        List<String> listOfResult = SeparationData.readFromFile("D:\\Confidentially\\Assessors forms\\Features1_19_RelianceAndTime\\SMOreg.txt");
        Map<String, Double> realMap = new TreeMap<>();
        Map<String, Double> precisionMap = new TreeMap<>();
        for (String line : listOfResult) {
            String[] p;
            p = line.split(" ");

            realMap.put(p[0], Double.valueOf(p[1]));
            precisionMap.put(p[0], Double.valueOf(p[2]));

        }

        final Map<String, Double> sortedRealMap = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return realMap.get(lhs).compareTo(realMap.get(rhs));
            }
        });
        final Map<String, Double> sortedPrecisionMap = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return precisionMap.get(lhs).compareTo(precisionMap.get(rhs));
            }
        });

        sortedRealMap.putAll(realMap);
        sortedPrecisionMap.putAll(precisionMap);

        //смотрим на разность мест до и после обучения
        Map<Integer, List<String>> allocationSeats = new TreeMap<>();
        int seat = 1;
        List<String> listOFPeole = new ArrayList<>();

        for (final Map.Entry<String, Double> entryInSorted : sortedRealMap.entrySet()) {
            for (final Map.Entry<String, Double> entry : realMap.entrySet()) {
                if (entry.getValue().equals(entryInSorted.getValue())) {
                    listOFPeole.add(entry.getKey());
                }
            }
            allocationSeats.put(seat, listOFPeole);
            listOFPeole = new ArrayList<>();
            seat++;
        }
        System.out.println();
        System.out.println("Предсказанное распределение мест");
        for (final Map.Entry<Integer, List<String>> entry : allocationSeats.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        seat = 1;
        Map<Integer, String> allocationSeatsReal = new TreeMap<>();

        for (final Map.Entry<String, Double> entryInSorted : sortedPrecisionMap.entrySet()) {
            allocationSeatsReal.put(seat, entryInSorted.getKey());
            seat++;
        }
        List<String> realSeats = new LinkedList<>();
        System.out.println("Реальное распределение мест");
        for (final Map.Entry<Integer, String> entry : allocationSeatsReal.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
            realSeats.add(entry.getValue());
        }
        System.out.println("Разность мест");
        int tmpSize = 0;
        int commonErrorMethod = 0;
        for (final Map.Entry<Integer, List<String>> entry : allocationSeats.entrySet()) {

            for (String tmp : entry.getValue()) {
                tmpSize++;
                if ((realSeats.indexOf(tmp) + 1 <= entry.getValue().size() - 1 + tmpSize) && (realSeats.indexOf(tmp) + 1 >= tmpSize)) {
                    System.out.println("предсказание места для асессора " + tmp + " верное");
                } else {
                    if (realSeats.indexOf(tmp) + 1 > entry.getValue().size() - 1 + tmpSize) {
                        System.out.println("ошибка предсказания в " + (realSeats.indexOf(tmp) + 1 - (entry.getValue().size() - 1 + tmpSize)) + " место для асессора " + tmp);
                        commonErrorMethod = commonErrorMethod + (realSeats.indexOf(tmp) + 1 - (entry.getValue().size() - 1 + tmpSize));
                    }
                    if (realSeats.indexOf(tmp) + 1 < tmpSize - entry.getValue().indexOf(tmp)) {
                        System.out.println("ошибка предсказания в " + (tmpSize - entry.getValue().indexOf(tmp) - (realSeats.indexOf(tmp) + 1)) + " место для асессора " + tmp);
                        commonErrorMethod = commonErrorMethod + (tmpSize - entry.getValue().indexOf(tmp) - (realSeats.indexOf(tmp) + 1));
                    }
                }

            }

        }

        System.out.println("суммарная ошибка определения мест для метода = " + commonErrorMethod);

    }

}
